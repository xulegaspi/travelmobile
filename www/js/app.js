

var myApp = angular.module('starter', ['ionic', 'starter.controllers', 'starter.directives', 'starter.services'])

.run(function($ionicPlatform) {
  //$ionicPlatform.ready(function() {
  //  if(window.StatusBar) {
  //    StatusBar.styleDefault();
  //  }
  //});
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {
    //openFB.init({appId: '1546145212327993'});
    $stateProvider

        .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "templates/menu.html",
            controller: 'AppCtrl'
        })

        //.state('app.assignment', {
        //    url: "/assignment",
        //    views: {
        //        'menuContent': {
        //            templateUrl: "templates/assignment.html",
        //            controller: 'AssignmentCtrl'
        //        }
        //    }
        //})

        .state('app.camera', {
            url: "/camera",
            views: {
                'menuContent': {
                    templateUrl: "templates/camera.html",
                    controller: 'CameraCtrl'
                }
            }
        })

        .state('app.start', {
            url: "/start",
            views: {
                'menuContent': {
                    templateUrl: "templates/start.html",
                    controller: 'StartCtrl'
                }
            }
        })

        .state('app.map', {
            url: "/map",
            views: {
                'menuContent': {
                    templateUrl: "templates/gmaps.html",
                    controller: 'MapCtrl'
                }
            }
        });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/start');
});