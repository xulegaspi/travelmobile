angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout, $http) {
// Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
        $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);

        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
            $scope.closeLogin();
        }, 1000);

        $http.post('https://protected-everglades-4257.herokuapp.com/api/login', {user: $scope.loginData.username, pass: $scope.loginData.password})
            .success(function(data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
            })
            .error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    };
})

.controller('StartCtrl', function($scope) {

})

.controller('LoginCtrl', function($scope) {

})

.controller('CameraCtrl', function($scope, $http, Camera) {

    $scope.lastPhoto = window.localStorage['Photo'];

    $scope.getPhoto = function() {
        //navigator.camera.getPicture(function(imageURI) {
        Camera.getPicture().then(function(imageURI) {
            console.log(imageURI);
            $scope.lastPhoto = imageURI;
            window.localStorage['Photo'] = imageURI;
            $scope.getLocation();
            var fd = new FormData();
            fd.append('image', imageURI);
            $http.post('https://protected-everglades-4257.herokuapp.com/api/images', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function() {
                console.log("Post image successful");
            }).error(function() {
                console.log("Post ERROR");
            });
        }, function(err) {
            console.err(err);
        }, {
            quality: 75,
            targetWidth: 320,
            targetHeight: 320,
            saveToPhotoAlbum: false
        });
    };

    $scope.clear = function() {
        //window.localStorage['Photo'] = $scope.lastPhoto;
        window.localStorage.clear();
    };

    $scope.getLocation = function() {
        navigator.geolocation.getCurrentPosition(function (pos) {
            //console.log('Got pos', pos);
            alert(pos);
            //new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
            window.localStorage['GeoLocation'] = '{"Latitude": ' + pos.coords.latitude + '; "Longitude": ' + pos.coords.longitude + '}';
        }, function (error) {
            alert('Unable to get location: ' + error.message);
        });
    };
})

.controller('MapCtrl', function($scope, $ionicLoading) {
  $scope.mapCreated = function(map) {
    $scope.map = map;
  };

  $scope.centerOnMe = function () {
    console.log("Centering");
    if (!$scope.map) {
      return;
    }

    $scope.loading = $ionicLoading.show({
      content: 'Getting current location...',
      showBackdrop: false
    });

    navigator.geolocation.getCurrentPosition(function (pos) {
      console.log('Got pos', pos);
      $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      $scope.loading.hide();
    }, function (error) {
      alert('Unable to get location: ' + error.message);
    });
  };
});
